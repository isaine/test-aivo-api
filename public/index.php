<?php
/**
 * Created by Isané Duque.
 * Date: 02/03/2018
 * Name Project: Test API AIVO
 */

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;


//importando autload de slim y facebook
require '../vendor/autoload.php';
require '../src/Facebook/autoload.php';

//instanciando variable slim
$app = new \Slim\App;


//API endpoint para facebook
$app->get('/profile/facebook/{id}', function (Request $request, Response $response, array $args) {


		//Identificador de la aplicaicoón API-AIVO-TEST
		$app_id = '164950160969167';
		$appi_secret = '42161eb5fe5985b7b8b28d0c38336193';
		$token = 'EAACWBXPADc8BAP1e1RlBrsHKoCgaqZBOv9iTkyEzsuZAZBnEV3AeQGJDKBZB3LF6jQeZAu6DKFX2eU7lBfrfIH7aEtczaMIaUZBRsIC1t6uyPyGVPZC9nZAw0jkLl4XPCQUZAseyKFtySqBoFmk6VtS1rv1emAg6zEiYZD';	

		//Instanciando objeto de facebook
		$fb = new Facebook\Facebook([
		  'app_id' => $app_id ,
		  'app_secret' => $appi_secret,
		  'default_graph_version' => 'v2.12',
		 ]);

		//Parámetro de la petición por HTTP
		$id = $args['id'];


		// Integración con Facebook
		try {
		  // Returns a `FacebookFacebookResponse` object
		  $response = $fb->get(
		    '/'.$id.'?fields= id,name,first_name,last_name',$token		    
		  );
		} catch(FacebookExceptionsFacebookResponseException $e) {
		  echo 'Graph returned an error: ' . $e->getMessage();
		  exit;
		} catch(FacebookExceptionsFacebookSDKException $e) {
		  echo 'Facebook SDK returned an error: ' . $e->getMessage();
		  exit;
		}

		$user = $response->getGraphNode();

		//Creando array con la información
		$objetofb = array(
	        'id'=> $user['id'],
	        'firtsname'=> $user['first_name'],
	        'lastName'=> $user['last_name'],
        );

	    //imprimiendo array
	    echo json_encode($objetofb);

		});
$app->run();